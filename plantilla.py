#!/usr/bin/env pythoh3

'''
Busca una palabra en una lista de palabras
'''

import sortwords

def search_word(word, words_list):
    lower_bound = 0
    upper_bound = len(words_list) - 1
    found = False
    index = -1

    while lower_bound <= upper_bound:
        mid = (lower_bound + upper_bound) // 2
        if words_list[mid].lower() == word.lower():
            found = True
            index = mid
            break
        elif words_list[mid].lower() < word.lower():
            lower_bound = mid + 1
        else:
            upper_bound = mid - 1
    return index if found else -1


def main():
    words = ['hola', 'adios', 'que tal ']
    ordered_list = sortwords.sort(words)
    search_word_result = search_word('hola', ordered_list)
    sortwords.show(ordered_list)
    print(f'position de hola es {search_word_result}')


if __name__ == '__main__':
    main()
